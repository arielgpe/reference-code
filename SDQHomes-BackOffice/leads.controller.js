/**
 * Created by Ariel Guzman on 2/3/2016
 */
(function () {

  angular
    .module('app.pages')
    .controller('leadsController', LeadsController);

  LeadsController.$inject = ['$scope', '$log', '$state', '$uibModal', 'Lead', 'Building', 'DTOptionsBuilder', 'SweetAlert', '$translate'];
  function LeadsController($scope, $log, $state, $uibModal, Lead, Building, DTOptionsBuilder, SweetAlert, $translate) {
    var vm = this;

    activate();

    function activate() {

      $scope.radioModel = 'All';
      vm.searchTerm = "";
      var query = {};
      var limit = 10;
      var offset = 0;
      vm.totalItems = 0;
      vm.itemsPerPage = 10;
      var order = "status ASC";

      vm.findContacts = function () {
        vm.currentPage = 1;
        query["type"] = 1;
        vm.currentTab = 1;
        offset = 0;
        vm.contacts = vm.findLeads();
      };

      vm.findInvestors = function () {
        vm.currentPage = 1;
        query["type"] = 2;
        vm.currentTab = 2;
        offset = 0;
        vm.investors = vm.findLeads();
      };

      vm.findCalls = function () {
        vm.currentPage = 1;
        query["type"] = 3;
        vm.currentTab = 3;
        offset = 0;
        vm.calls = vm.findLeads();
      };

      vm.findOwners = function () {
        vm.currentPage = 1;
        query["type"] = 4;
        vm.currentTab = 4;
        offset = 0;
        vm.owners = vm.findLeads();
      };

      vm.findNews = function () {
        if (query.hasOwnProperty("type")){
          delete query.type;
        }
        query["status"] = 0;
        vm.currentPage = 1;
        vm.currentTab = 5;
        offset = 0;
        vm.news = vm.findLeads();
      };


      vm.findLeads = function () {
        Lead.count({where: query}, function(response){
          vm.totalItems = response.count;
        });

        return Lead.find({
          filter: {
            order: order,
            limit: limit,
            offset: offset,
            include: 'account',
            where: query
          }
        })
      };

      function reset(){
        if (vm.currentTab == 1){
          vm.contacts = vm.findLeads();
        } else if (vm.currentTab == 2){
          vm.investors = vm.findLeads();
        } else if (vm.currentTab == 3) {
          vm.calls = vm.findLeads();
        } else if (vm.currentTab == 4) {
          vm.owners = vm.findLeads();
        }else if (vm.currentTab == 5) {
          vm.news = vm.findNew();
        }
      }

      vm.findContacts();

      vm.pageChange = function(){
        offset = (vm.currentPage - 1) * vm.itemsPerPage;
        reset();
      };


      $scope.toggleActive = function (isActive) {
        if (isActive == 'active') {
          query["status"] = {neq: 9};
          $scope.radioModel = 'Active';
        } else if (isActive == 'inactive') {
          query["status"] = 9;
          $scope.radioModel = 'Inactive';
        } else {
          if (query.hasOwnProperty("status")){
            delete query.status;
          }
          $scope.radioModel = 'All';
        }

        offset = 0;
        vm.currentPage = 1;
        reset();
      };

      vm.addLead = function (size) {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/views/templates/leads/add.html',
          controller: 'addLeadController',
          controllerAs: "lc",
          size: size
        });


        modalInstance.result.then(function (lead) {
          reset();
        });
      };

      vm.edit = function (size, lead) {
        console.log("lead to edit ", lead);
        var modalInstance = $uibModal.open({
          templateUrl: 'app/views/templates/leads/edit.html',
          controller: 'editLeadController',
          controllerAs: "lc",
          size: size,
          resolve: {
            lead: function () {
              return angular.copy(lead);
            }
          }
        });

        modalInstance.result.then(function (lead) {
          reset();
        });

      };

      vm.assign = function (size, lead) {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/views/templates/leads/assign.html',
          controller: 'assignLeadController',
          controllerAs: "lc",
          size: size,
          resolve: {
            lead: function () {
              return angular.copy(lead);
            }
          }
        });

        modalInstance.result.then(function (lead) {
          reset();
        });
      };


      vm.setActive = function (lead) {
        var isActive;
        if (lead.status == 9) {
          isActive = 0;
        }
        else if (lead.status < 9) {
          isActive = 9;
        }
        // var isActive = !leads.status;
        Lead.prototype$updateAttributes({id: lead.id}, {status: isActive}, function (lead) {
           $translate(['leads.updated', 'leads.updated']).then(function (translations) {
            var updated = translations['leads.updated'];
            SweetAlert.swal(updated);
            reset();
          });

        });
      };

      /**
       * @return {boolean}
       */
      function IsNumeric(input){
        var RE = /^-{0,1}\d*\.{0,1}\d+$/;
        return (RE.test(input));
      }

      var numericFields = [
        "id", "status","budget"
      ];

      var stringFields = [
        "name", "phone", "interest", "sent"
      ];

      vm.searchChanged = function (searchTerm) {
        var isNum = IsNumeric(searchTerm);
        if (searchTerm == "") {
          delete query.or;
        } else {
          query["or"] = [];
          if (isNum) {

            numericFields.forEach(function (field, index) {
              var object = {};
              object[field] = {inq: [Number(searchTerm)]};
              query["or"][index] = object;
            });
          } else {
            stringFields.forEach(function(field, index){
              var object = {};
              object[field] = {like: searchTerm+'.+st'};
              query["or"][index] = object;
            })
          }
        }

        offset = 0;
        vm.currentPage = 1;
        reset();
      };
    }
  }
})();