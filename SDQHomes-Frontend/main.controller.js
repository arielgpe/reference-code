/**
 * Created by Ariel Guzman on 2/3/2016
 */
(function () {

  angular
    .module('app.main')
    .controller('MainController', MainController);

  MainController.$inject = ['$scope', '$log', '$state', '$location', '$anchorScroll', 'SectorFind', 'Listing', '$localStorage'];
  function MainController($scope, $log, $state, $location, $anchorScroll, SectorFind, Listing, $localStorage) {
    var vm = this;
    var saleIds = [];
    var rentalId = [];
    vm.searchType = "buy";


    activate();


    function activate() {
      vm.goToLink = function (id) {
        $location.hash(id);
        $anchorScroll();
      };

      SectorFind.get(function (sectors) {
        vm.sectors = [];
        vm.allSectors = [];
        vm.buyListings = [];
        vm.rentListings = [];
        var sectorIds = [];

        sectors.forEach(function (sector) {
          if (sector.featured == true){
            vm.sectors.push(sector);
          }
          vm.allSectors.push(sector);
          sectorIds.push(sector.id)
        });

        rentProperties(sectorIds);
        buyProperties(sectorIds);
      });

      vm.searchSector = function(){
        if(vm.selectedSector){
          $localStorage.searchSector = vm.selectedSector.name.split(' ').join('-');
          if (vm.searchType == "buy"){
            $state.go('app.buy', {sector: vm.selectedSector.name.split(' ').join('-')})
          } else {
            $state.go('app.rent', {sector: vm.selectedSector.name.split(' ').join('-')})
          }
        } else {
          if (vm.searchType == "buy"){
            $state.go('app.buy')
          } else {
            $state.go('app.rent')
          }
        }
      }
    }

    function buyProperties(sectorIds) {
      shuffle(sectorIds);
      var firstSec = sectorIds[0];
      var scndSec = sectorIds[1];
      var thirdSec = sectorIds[2];
      var fourthSec = sectorIds[3];

      var minArea = getRandomArbitrary(1, 300);
      var maxArea = getRandomArbitrary(350, 10000);
      var minPrice = getRandomArbitrary(500, 1000000);
      var maxPrice = getRandomArbitrary(1000001, 100000000);

      Listing.find({
        filter: {
          limit: 4,
          include: ["sector", {relation: "files", scope: {where: {order: 0}}}],
          "where": {
            id: {
              "nin": saleIds
            },
            "contractType": {"inq": [1, 3]}, "or": [{"sectorId": firstSec}, {"sectorId": scndSec},
              {"sectorId": thirdSec}, {"sectorId": fourthSec}], "area": {"between": [minArea, maxArea]},
            "sale": {"between": [minPrice, maxPrice]}
          }
        }
      }, function (listings) {
        listings.forEach(function (lst) {
          saleIds.push(lst.id);
          vm.buyListings.push(lst);
        });
        if (vm.buyListings.length < 4) {
          buyProperties(sectorIds)
        }
      })
    }

    function rentProperties(sectorIds) {
      shuffle(sectorIds);
      var firstSec = sectorIds[0];
      var scndSec = sectorIds[1];
      var thirdSec = sectorIds[2];
      var fourthSec = sectorIds[3];

      var minArea = getRandomArbitrary(1, 100);
      var maxArea = getRandomArbitrary(101, 10000);
      var minPrice = getRandomArbitrary(0, 100);
      var maxPrice = getRandomArbitrary(1001, 100000000);

      Listing.find({
        filter: {
          limit: 10,
          include: ["sector", {relation: "files", scope: {where: {order: 0}}}],
          "where": {
            id: {
              "nin": rentalId
            },
            "contractType": {"inq": [2, 3]},
            "or": [{"sectorId": firstSec}, {"sectorId": scndSec},
              {"sectorId": thirdSec}, {"sectorId": fourthSec}],
            "area": {"between": [minArea, maxArea]},
            "rental": {"between": [minPrice, maxPrice]}
          }
        }
      }, function (listings) {
        listings.forEach(function (lst) {
          rentalId.push(lst.id);
          vm.rentListings.push(lst);
        });
        if (vm.rentListings.length < 4) {
          rentProperties(sectorIds)
        }
      })
    }


    function getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }


    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }
  }
})();